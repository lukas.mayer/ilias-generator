
__title__ = 'hallgrimJS'
__version__ = '0.4.0'
__author__ = 'Jan Maximilian Michal, Lukas Mayer'
__license__ = 'MIT'
__copyright__ = 'Copyright 2016 Jan Maximilian Michal'

# for the executable
from .hallgrimJS import parseme
