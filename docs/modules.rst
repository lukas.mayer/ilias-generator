Source code documentation
*************************

These are just the docstrings for the classes and functions that differ from The 
original version of Hallgrim. You could just read the code itself.

Module ``hallgrimJS.hallgrimJS``
================================

.. automodule:: hallgrimJS.hallgrimJS
    :members: handle_javaScript_questions

Module ``hallgrimJS.IliasXMLCreator.javaScript``
================================================

.. automodule:: hallgrimJS.IliasXMLCreator.javaScript
    :members: